﻿//var FHIR_URL = 'http://mobilars.apphb.com/fhir';
var FHIR_URL = 'http://fhir.roland.bz';


angular.module('fhirApp.controllers', [])
    .controller('ObsController', ['$scope', '$q', 'BLE', 'Settings', '$interval',
        function ($scope, $q, BLE, Settings, $interval) {

            $scope.devices = BLE.getDevices();

            $scope.settings = Settings.getSettings();
            $scope.patientId = $scope.settings.patientId;

            $scope.BLE = BLE;

            $scope.sensorValues = BLE.getSensorValues();
            $scope.sensorMessage = BLE.getSensorMessage();

            $scope.state = Math.round(Math.random() * 100000000).toString();

            document.addEventListener("deviceready", onDeviceReady, false);
            function onDeviceReady() {
                console.log('onDeviceReady settings:' + JSON.stringify($scope.settings));
                $scope.scan();
            }

            $scope.startRecovery = function() {
                $scope.recoveryHeartRate = $scope.sensorValues.pulseHRM;
                $scope.recoveryStartTime = new Date();
                delete $scope.recoveryHeartRateOneMin;
                delete $scope.recoveryHeartRateTwoMins;
                console.log('recoveryStartTime = '+$scope.recoveryStartTime);
                $interval(function() {
                    var now = new Date();
                    //console.log('recoveryStartTime = '+$scope.recoveryStartTime);
                    //console.log('now = '+now);
                    $scope.secondsPassed = parseInt((now.getTime() - $scope.recoveryStartTime.getTime()) / 1000);
                    if ($scope.secondsPassed == 60) {
                        $scope.recoveryHeartRateOneMin = $scope.sensorValues.pulseHRM;
                    }
                    if ($scope.secondsPassed == 120) {
                        $scope.recoveryHeartRateTwoMins = $scope.sensorValues.pulseHRM;
                    }
                },
                    500, // Called every 1/2 sec
                    600 // stops after 60 seconds
                );
            };

            $scope.saveHRMStreaming = function() {
                $scope.saveHRM($scope.sensorValues.pulseHRM);
                $scope.streamingInterval = $interval(function() {
                        var now = new Date();
                        $scope.saveHRM($scope.sensorValues.pulseHRM);
                    },
                    60000 // Called every minute
                    );
            };

            $scope.stopHRMStreaming = function() {
                $interval.cancel($scope.streamingInterval);
                delete $scope.streamingInterval;
            };

            $scope.getPatientInfo = function (defer) {
                var url = $scope.settings.serviceUri + "/Patient/" + $scope.patientId;
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    beforeSend: function (xhr) {
                        //xhr.setRequestHeader("Authorization", "Bearer " + $scope.accessToken);
                    }
                })
                    .done(function (pt) {
                        $scope.$apply(function () {
                            $scope.patient = pt;
                            console.log('patient = ' + JSON.stringify($scope.patient));
                            var name = pt.name[0].given.join(" ") + " " + pt.name[0].family.join(" ");
                            $scope.patientName = name;
                            console.log('patient = ' + $scope.patientName);
                            $scope.loggedIn = true;
                            defer.resolve();
                        });
                    })
                    .fail(function (error) {
                        console.log('Error getting patient:' + error + ',' + JSON.stringify(error));
                        defer.reject(-1);
                    })
                ;
            };

            $scope.saveHRM = function (pulse) {
                $scope.message = 'Saving data';

                var jsonPulse = {
                    "resourceType": "Observation",
                    "status": "final",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">'+ pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "149530",
                                "display": "MDC_PULS_OXIM_PULS_RATE"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": pulse,
                        "unit": "bpm",
                        "system": "urn:std:iso:11073:10101",
                        "code": "264864"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };

                $scope.submitObservation(jsonPulse);

            };

            $scope.savePulseOximeterData = function (pId, Sp02, pulse) {
                $scope.message = 'Saving data';

                var jsonO2 = {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "150456",
                                "display": "MDC_PULS_OXIM_SAT_O2"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": Sp02,
                        "unit": "percent",
                        "system": "urn:std:iso:11073:10101",
                        "code": "262688"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };

                var jsonPulse = {
                    "resourceType": "Observation",
                    "status": "final",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + Sp02 + '%, ' + pulse + ' bpm heart rate</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "149530",
                                "display": "MDC_PULS_OXIM_PULS_RATE"
                            }
                        ]
                    },
                    "valueQuantity": {
                        "value": pulse,
                        "unit": "bpm",
                        "system": "urn:std:iso:11073:10101",
                        "code": "264864"
                    },
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };

                //console.log('Bearer:'+$scope.accessToken);
                //console.log('Saving data:'+JSON.stringify(json));

                $scope.submitObservation(jsonO2);
                $scope.submitObservation(jsonPulse);

            };

            $scope.submitObservation = function (json) {

                // TODO: Move to Angular. See formly-fhir project
                $.ajax({
                    type: "POST",
                    url: $scope.settings.serviceUri + '/Observation',
                    headers: {
                        //"Authorization":"Bearer "+$scope.accessToken
                    },
                    data: JSON.stringify(json),
                    success: function (data) {
                        console.log('From server:' + JSON.stringify(data));
                        $scope.$apply(function () {
                            $scope.message = 'Data saved to cloud. '+JSON.stringify(data.issue[0].diagnostics);
                        });
                    },
                    datatype: 'json',
                    contentType: 'application/json+fhir;charset=utf-8'
                }).fail(function (error) {
                    console.log('Save error:' + error + ',' + JSON.stringify(error));
                }).done(function (data) {
                    console.log("Returned success from server. Sample of data:" + JSON.stringify(data));
                });
            };

            $scope.submitBundle = function (json) {
                $.ajax({
                    type: "POST",
                    url: $scope.settings.serviceUri + '/Bundle',
                    headers: {
                        //"Authorization":"Bearer "+$scope.accessToken
                    },
                    data: JSON.stringify(json),
                    success: function (data) {
                        console.log('From server:' + JSON.stringify(data));
                        $scope.$apply(function () {
                            $scope.message = 'Data saved to cloud';
                        });
                    },
                    datatype: 'json',
                    contentType: 'application/json+fhir;charset=utf-8'
                }).fail(function (error) {
                    console.log('Save error:' + error + ',' + JSON.stringify(error));
                }).done(function (data) {
                    console.log("Sample of data:" + JSON.stringify(data));
                });
            };


            $scope.saveBP = function (pId, sys, dia) {
                $scope.message = 'Saving data';

                var jsonSys =
                {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\"> Systolic:' + sys + '</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "code": "150020",
                                "display": "MDC_PRESS_BLD_NONINV"
                            }
                        ]
                    },
                    "component": [
                        {
                            "code": {
                                "coding": [
                                    {
                                        "code": "150021",
                                        "display": "MDC_PRESS_BLD_NONINV_SYS"
                                    }
                                ]
                            },
                            "valueQuantity": {
                                "value": sys,
                                "unit": "MDC_DIM_MMHG",
                                "code": "266016"
                            }
                        },
                        {
                            "code": {
                                "coding": [
                                    {
                                        "code": "150022",
                                        "display": "MDC_PRESS_BLD_NONINV_DIA"
                                    }
                                ]
                            },
                            "valueQuantity": {
                                "value": dia,
                                "unit": "MDC_DIM_MMHG",
                                "code": "266016"
                            }
                        }
                    ],
                    "effectiveDateTime": new Date(),
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };


                //console.log('Bearer:'+$scope.accessToken);
                console.log('jsonSys:' + JSON.stringify(jsonSys));

                //$scope.submitBundle(json);
                $scope.submitObservation(jsonSys);

            };

            $scope.saveWeight = function (pId, weight) {
                $scope.message = 'Saving data';

                var json = {
                    "resourceType": "Observation",
                    "text": {
                        "status": "generated",
                        "div": '<div xmlns=\"http://www.w3.org/1999/xhtml\">' + weight + ' kg</div>'
                    },
                    "code": {
                        "coding": [
                            {
                                "system": "urn:std:iso:11073:10101",
                                "code": "188736",
                                "display": "MDC_MASS_BODY_ACTUAL"
                            }
                        ]
                    },
                    "effectiveDateTime": new Date(),
                    "valueQuantity": {
                        "value": weight,
                        "unit": "kg",
                        "system": "urn:std:iso:11073:10101",
                        "code": "263875"
                    },
                    "reliability": "ok",
                    "subject": {"reference": "Patient/" + $scope.patientId}
                };

                //console.log('Bearer:'+$scope.accessToken);
                console.log('Saving data:' + JSON.stringify(json));

                $scope.submitObservation(json);

            };

            $scope.saveSettings = function () {
                Settings.saveSettings();
            };

            $scope.exit = function () {
                app.exitApp();
            };

            $scope.scan = function () {
                console.log('Starting scan');
                BLE.scan();
            };

            $scope.connect = function (connectTo) {

                console.log('Connecting to: ' + connectTo);

                BLE.connect(connectTo);

            };

            $scope.disconnect = function () {
                BLE.disconnect();
                //bleservice.reset();
                $scope.exit();
            };

        }]);
